/**
 * Created by remziyesahin on 01.08.2019.
 */
public class Hobby {
    private int id ;
    private String name;

    public Hobby(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
