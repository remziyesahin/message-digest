import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by remziyesahin on 01.08.2019.
 */
public class SHA256Summary {
    private static Logger logger = Logger.getLogger(SHA256Summary.class.getName());
    private MessageDigest md;

    public void resetDigest()  {

        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            md = null;
        }
    }
 
    public String getDigest()  {

        if(md == null){
            return null;
        }
        return Base64.getEncoder().encodeToString(md.digest()); 
    }

    public void  makeDigest(String ... content)  {

        if(md == null){
            return;
        }
        for (int i = 0; i< content.length; i++) {
            try {
                if(content[i]!=null){
                    md.update(content[i].getBytes("UTF-8"));
                }
            } catch (UnsupportedEncodingException e) {
                logger.log(Level.SEVERE, "An error occured.", e);
            }
        }
    }
}
