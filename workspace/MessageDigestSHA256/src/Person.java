import java.util.ArrayList;
import java.util.List;

/**
 * Created by remziyesahin on 01.08.2019.
 */
public class Person {
    private String name;
    private String surname;
    private int age;
    private String gender;

    private List<Hobby> hobbies ;


    public Person(String name, String surname, int age, String gender,  List<Hobby> hobbies ) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.gender = gender;
        this.hobbies = hobbies;
    }

    public Person(Hobby hobby) {
        this.hobbies = new ArrayList<>();
        hobbies.add(hobby);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<Hobby> getHobbies() {
        return hobbies;
    }

    public void setHobbies(List<Hobby> hobbies) {
        this.hobbies = hobbies;
    }

    public void addHobby(Hobby hobby){
        hobbies.add(hobby);
    }
}
