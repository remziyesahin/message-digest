import sun.java2d.ScreenUpdateManager;

import java.util.ArrayList;

/**
 * Created by remziyesahin on 01.08.2019.
 */
public class Test {
    public static void main(String[] args) {
        SHA256Summary summary = new SHA256Summary();
        Person person =new Person("Remziye","Sahin",30,"Female",new ArrayList<Hobby>());
        person.addHobby(new Hobby(1,"Cinema"));
        person.addHobby(new Hobby(2,"Paragliding"));
        person.addHobby(new Hobby(3,"Reading"));

        // It zeroizes the message digest
        summary.resetDigest();
        // Passing the hash algorithm respectively.
        summary.makeDigest(person.getName(),person.getSurname(), String.valueOf(person.getAge()),person.getGender());
        for (Hobby hobby : person.getHobbies()){
            summary.makeDigest(String.valueOf(hobby.getId()),hobby.getName());
        }
        String result  = summary.getDigest();
        String b64sha256TestCase = "7s6d/zFj7Wa6VX1yXyX1mdEgOUP/D8SScbOZu9Goxxk=";
        if(result.equals(b64sha256TestCase)){
            System.out.println("Success!");
            System.out.println("Result = " + result);
        }else{
            System.out.println("Failed to verify summary.");
        }

    }
}
