All parameters in the request must be passed through the hash algorithm, respectively.
SHA256 cryptographic summary of all fields of the request,  encoded in Base 64. 
The text representations of the message parameters will be summarized in the order in which they appear in the message.